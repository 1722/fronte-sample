import axios from 'axios'
import config from './config'

import { LocalStorage } from 'quasar'

export default {
  auth (data) {
    console.log('auth data: ', data)
    return axios.post(config.api + 'auth', {
      email: data.email,
      username: data.username,
      password: data.password
    })
      .then((res) => {
        console.log('auth: ', res.response)
        let token = res.data.token
        // let drawer = res.data.links
        LocalStorage.set('token', token)
        return res.data
      })
      .catch((err) => {
        return err.response
      })
  }
}
